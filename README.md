# Exercice 3 : CICD

## Consigne :

- Créer un dépôt privé GitLab et le partager avec le compte devopssenegal. 
- Attribuer le rôle "maintainer" au compte devopssenegal
- Permettre la création automatique d'image docker pour l'application web 
- Stocker les images dans GitLab Container Registry de votre dépot
- Créer un fichier docker compose utilisant la dernière version de votre image (celle se trouvant dans votre registry gitlab)